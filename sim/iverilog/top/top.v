module main;

reg clk, reset_n;

initial begin
    $dumpfile("top.vcd");
    $dumpvars;
end

ao68000 cpu(
    .CLK_I(clk),
    .reset_n(reset_n),

    .CYC_O(),
    .ADR_O(),
    .DAT_O(),
    .DAT_I(32'b0),
    .SEL_O(),
    .STB_O(),
    .WE_O(),
    .ACK_I(1'b0),
    .ERR_I(1'b0),
    .RTY_I(1'b0),
    .SGL_O(),
    .BLK_O(),
    .RMW_O(),
    .CTI_O(),
    .BTE_O(),
    .fc_o(),
    .ipl_i(3'b0),
    .reset_o(),
    .blocked_o()
);

initial begin
    #0 clk <= 1;
    reset_n <= 0;

    #200 reset_n <= 1;
    #10000 $finish;
end

always #50 clk <= ~clk;

endmodule // main
