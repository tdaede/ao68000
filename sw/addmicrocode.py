#!/bin/env python
import sys
from textwrap import dedent

verilogin = open(sys.argv[1], "r")
with open(sys.argv[2], "r") as mif:
    # Ignore header lines and END line
    miflines = mif.readlines()[6:-1]
verilogout = open(sys.argv[3], "w")

for line in verilogin:
    if line == "//add microcode here\n":
        verilogout.write(
            dedent("""
                reg [87:0] microcode[0:{}];
                initial begin
            """).format(len(miflines)-1)
        )
        for line2 in miflines:
            verilogout.write(
                "    microcode[{}] = 88'b{};\n".format(
                    int(line2[:6]), line2[8:96]
                )
            )
        verilogout.write("end\n")
    else:
        verilogout.write(line)

verilogin.close()
verilogout.close()

