import os

from nmigen import *
from nmigen.lib.io import *

from nmigen_soc.wishbone.bus import Interface

__all__ = ["ao68000"]

class ao68000(Elaboratable):
    @staticmethod
    def _add_files(platform, prefix):
        d = os.path.realpath("{dir}{sep}{par}{sep}verilog".format(
            dir=os.path.dirname(__file__), sep=os.path.sep, par=os.path.pardir
        )) + os.path.sep
        for fname in [
            "alu_mult_generic.v",
            "memory_registers_generic.v",
            "ao68000.v",
        ]:
            f = open(d + fname, "r")
            platform.add_file(prefix + fname, f)
            f.close()


    def __init__(self, *, with_stall=False):
        self.sgl = Signal()
        self.blk = Signal()
        self.rmw = Signal()
        self.fc = Signal(3)
        self.ipl = Signal(3)
        self.blocked = Signal()
        self.en = Signal(reset=1) # In false chip will be put in reset state

        feats = ("lock", "err", "rty", "cti", "bte")
        if with_stall:
            feats += ("stall",)
        self.bus = Interface(
            data_width=32, addr_width=30, granularity=8,
            features=feats,
        )

    
    def elaborate(self, platform):
        self.__class__._add_files(platform, self.__class__.__name__ + os.path.sep)

        m = Module()

        wb = self.bus
        stb = Signal()
        reset_n = Signal()

        m.d.comb += reset_n.eq(~(ResetSignal() | ~self.en))

        m.submodules.m68k = m68k = Instance("ao68000",
            i_CLK_I = ClockSignal(),
            i_reset_n = reset_n,
            o_CYC_O = wb.cyc,
            o_ADR_O = wb.adr,
            o_DAT_O = wb.dat_w,
            i_DAT_I = wb.dat_r,
            o_SEL_O = wb.sel,
            o_STB_O = stb,
            o_WE_O = wb.we,
            i_ACK_I = wb.ack,
            i_ERR_I = wb.err,
            i_RTY_I = wb.rty,
            o_CTI_O = wb.cti,
            o_BTE_O = wb.bte,
            o_SGL_O = self.sgl,
            o_BLK_O = self.blk,
            o_RMW_O = self.rmw,
            o_fc_o = self.fc,
            i_ipl_i = ~self.ipl,
            o_blocked_o = self.blocked,
        )
        # Lock bus in either block burst or rmw cycle
        m.d.comb += wb.lock.eq(self.blk | self.rmw)

        # Implement stall generation
        if not hasattr(wb, "stall"):
            m.d.comb += wb.stb.eq(stb)
        else:
            with m.FSM():
                with m.State("IDLE"):
                    m.d.comb += wb.stb.eq(stb)
                    with m.If(wb.stb & ~wb.stall):
                        m.next = "WAIT4ACK"
                with m.State("WAIT4ACK"):
                    m.d.comb += wb.stb.eq(0)
                    with m.If(wb.ack | wb.err | wb.rty):
                        m.next = "IDLE"

        return m
