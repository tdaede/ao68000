import cocotb
from cocotb.triggers import Timer
from cocotb.binary import BinaryValue

@cocotb.coroutine
def clk_cycle(clk, quartperiod, cycles=1):
    for i in range(cycles):
        clk <= 0
        yield quartperiod
        clk <= 1
        yield quartperiod
        yield quartperiod
        clk <= 0
        yield quartperiod


@cocotb.test()
def test01(dut):
    """
    Test functionality
    """

    # Put the clock period to 1000 simulation steps
    quartperiod = Timer(250)

    dut.dataa <= BinaryValue("00000000000000000")
    dut.datab <= BinaryValue("00000000000000000")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.result == BinaryValue("00000000000000000000000000000000")

    dut.dataa <= BinaryValue("00000000000000001")
    dut.datab <= BinaryValue("00000000000000000")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.result == BinaryValue("00000000000000000000000000000000")

    dut.dataa <= BinaryValue("00000000000000001")
    dut.datab <= BinaryValue("00000000000000001")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.result == BinaryValue("00000000000000000000000000000001")

    dut.dataa <= BinaryValue("00000000000000010")
    dut.datab <= BinaryValue("00000000000000010")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.result == BinaryValue("00000000000000000000000000000100")

    dut.dataa <= BinaryValue("10000000000000000")
    dut.datab <= BinaryValue("10000000000000000")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.result == BinaryValue("0100000000000000000000000000000000")

    dut.dataa <= BinaryValue("10000000000000001")
    dut.datab <= BinaryValue("10000000000000001")

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.result == BinaryValue("0011111111111111100000000000000001")
